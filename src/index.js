import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import MyParentComponent from './Inline'
import Comments from './InlineEditable'

ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<MyParentComponent />, document.getElementById('inline'));
ReactDOM.render(<Comments />, document.getElementById('comment'));
registerServiceWorker();
