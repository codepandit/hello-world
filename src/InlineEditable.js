import React from "react";
import InlineEditable from "react-inline-editable-field";

export default class Comments extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {}
    }

    updateListing(isChanged,val){
        this.setState({myValue: val});
        console.log("New value=>"+ val);
    }

    render() {
        return (
            <div>
                <h1>
                    <InlineEditable
                        content={this.state.myValue}
                        inputType = "text"
                        displayPlaceholder="Enter text here.."
                        onBlur={(val, isChanged) => {this.updateListing(isChanged, val)}}
                        style={{width: '500px'}}
                        inputStyle={{width: '500px'}}
                        className = "customClassName"
                    />
                </h1>
            </div>
        );
    }
}